MuseumApp.Views.MainView = Backbone.View.extend({
	events: {
		'click #buttonRegister': 'registerUsers',
		'click #buttonLogin': 'logInUsers',
		'click .logoutBtn': 'logOutUsers',
		'keypress #loginPassword': 'onLoginKeypress'
	},
	initialize: function() {

		
	},
	render: function() {


		var currentUser = Parse.User.current();

		if (currentUser) {
			this.$('.logoutBtn').show();
			this.$('.regBtn, .loginBtn').hide();
			this.$('.clock').show();
			this.$('.launchMessage').show();
		


		} else {
			this.$('.logoutBtn').hide();
			this.$('.loginBtn, .regBtn').show();
			this.$('.clock').hide();
			this.$('.launchMessage').hide();

		}

		return this;
	},

	//Register Users  Fang
	registerUsers: function(){
		var user = new Parse.User();
		var that = this;
		// console.log('buttonRegister clicked');
		//Save New Users
		user.set({
			registerFirstname: this.$('#registerFirstname').val(),
			registerLastname: this.$('#registerLastname').val(),
			email: this.$('#registerEmail').val(),
			username: this.$('#registerEmail').val(),
			password: this.$('#registerPassword').val()
		});

		//Sign Up

		// Preferred
		user.signUp(null, {

		}).then(function(user) {

		}, function(user,error) {
			alert("Error: " + error.code + '' + error.message);

		}).then(function(){
			this.$('#giftItems').show();   //when logged out, it will turn to hide, so i need to show it 
			this.$('#userRegisterModal').modal('hide');
			this.$('.regBtn').hide();
			this.$('.loginBtn').hide();
			this.$('.logoutBtn').show();
		});
	},
	onLoginKeypress: function(event) {
		if (event.keyCode === 13) {
			this.logInUsers();
		}
	},

	// log initialize
	logInUsers:function(){
		var myUserName = $('#loginEmail').val();
		var myPass = $('#loginPassword').val();
		// console.log(myUserName + '' + myPass);
		var that = this;

		Parse.User.logIn(myUserName, myPass).then(function(user) {
			console.log('login successful: ' + user.get('username'));
			console.log('login successful: ' + user.getUsername());
			console.log('login successful: ' + user.username);

			that.$('#userLogInModal').modal('hide');

			that.render();	

		}, function(user, error) {
			alert('username or password is invalid!');
			// console.log("User (error): ", user);
			// console.log("Error: " + error.code + '' + error.message);
		});
	},
	logOutUsers:function(){
		Parse.User.logOut();
		this.render();
		alert('logged out successfully!');
	}

});


